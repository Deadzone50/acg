#include "PathTraceRenderer.hpp"
#include "RayTracer.hpp"
#include "AreaLight.hpp"

#include <atomic>
#include <chrono>
#include <algorithm>
#include <string>



namespace FW {

	bool PathTraceRenderer::m_normalMapped = false;
	float PathTraceRenderer::DofOffset = 0;
	float PathTraceRenderer::DofPlaneDist = 1;
	bool PathTraceRenderer::debugVis = false;
	bool PathTraceRenderer::specularsEnabled = false;
	bool PathTraceRenderer::BDPTEnabled = false;

	void PathTraceRenderer::getTextureParameters(const RaycastResult& hit, Vec3f& diffuse, Vec3f& n, Vec3f& specular)
	{
		MeshBase::Material* mat = hit.tri->m_material;
		// YOUR CODE HERE (R1)
		// Read value from albedo texture into diffuse.
	    // If textured, use the texture; if not, use Material.diffuse.
	    // Note: You can probably reuse parts of the radiosity assignment.

		Vec2f uv = (1 - hit.u - hit.v)*hit.tri->m_vertices[0].t + hit.u*hit.tri->m_vertices[1].t + hit.v*hit.tri->m_vertices[2].t;

		Texture& NormalMap = mat->textures[MeshBase::TextureType_Normal];
		if(NormalMap.exists() && m_normalMapped)				//check whether material uses a normal map
		{
			//Vec2f uv = (1 - hit.u - hit.v)*hit.tri->m_vertices[0].t + hit.u*hit.tri->m_vertices[1].t + hit.v*hit.tri->m_vertices[2].t;

			//deltaPos1 = deltaUV1.x * T + deltaUV1.y * B			//http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/
			//deltaPos2 = deltaUV2.x * T + deltaUV2.y * B
			//B = (deltapos1 - deltaUV1.x *T)/deltaUV1.y
			//B = (deltapos2 - deltaUV2.x *T)/deltaUV2.y
			//(deltapos1 - deltaUV1.x*T)/deltaUV1.y =  (deltapos2 - deltaUV2.x*T) /deltaUV2.y
			//deltapos1*deltaUV2.y - deltaUV1.x*T*deltaUV2.y =  deltapos2 *deltaUV1.y - deltaUV2.x*T *deltaUV1.y
			// deltaUV2.x*T *deltaUV1.y - deltaUV1.x*T*deltaUV2.y =  deltapos2 *deltaUV1.y -deltapos1*deltaUV2.y
			// deltaUV2.x*T *deltaUV1.y - deltaUV1.x*T*deltaUV2.y =  deltapos2 *deltaUV1.y -deltapos1*deltaUV2.y
			// T =  (deltapos1*deltaUV2.y - deltapos2 *deltaUV1.y) / (deltaUV1.x*deltaUV2.y - deltaUV2.x*deltaUV1.y)

			Vec3f v0 = hit.tri->m_vertices[0].p;
			Vec3f v1 = hit.tri->m_vertices[1].p;
			Vec3f v2 = hit.tri->m_vertices[2].p;

			Vec2f uv0 =  hit.tri->m_vertices[0].t;
			Vec2f uv1 =  hit.tri->m_vertices[1].t;
			Vec2f uv2 =  hit.tri->m_vertices[2].t;

			Vec3f deltaPos1 = v1-v0;		//edges
			Vec3f deltaPos2 = v2-v0;

			Vec2f deltaUV1 = uv1-uv0;		// UV delta
			Vec2f deltaUV2 = uv2-uv0;

			float r = 1.0f / (deltaUV1.x*deltaUV2.y - deltaUV1.y*deltaUV2.x);
			Vec3f T = (deltaPos1*deltaUV2.y - deltaPos2*deltaUV1.y)*r;
			Vec3f B = (-deltaPos2*deltaUV1.x - deltaPos1*deltaUV2.x)*r;

			T.normalize();
			B.normalize();

			Vec3f N = hit.tri->normal();
			
			Mat3f TangentToWorld;
			TangentToWorld.setCol(0, T);
			TangentToWorld.setCol(1, B);
			TangentToWorld.setCol(2, N);
			
			const Image& img = *NormalMap.getImage();
			Vec2i texelCoords = getTexelCoords(uv, img.getSize());
			Vec3f SN = TangentToWorld * img.getVec4f(texelCoords).getXYZ();
			SN.normalize();
			n = SN;
		}
		else
		{
			if(hit.tri->m_vertices[0].n.length() == 0)		//if the mesh has no normals defined
			{
				n = hit.tri->normal();
			}
			else
			{
				Vec3f SN = (1 - hit.u - hit.v)*hit.tri->m_vertices[0].n + hit.u*hit.tri->m_vertices[1].n + hit.v*hit.tri->m_vertices[2].n;		//interpolate normal
				SN.normalize();
				n = SN;
			}
		}


		Texture& diffuseTex = mat->textures[MeshBase::TextureType_Diffuse];
		if(diffuseTex.exists())				//check whether material uses a diffuse texture
		{
			//Vec2f uv = (1 - hit.u - hit.v)*hit.tri->m_vertices[0].t + hit.u*hit.tri->m_vertices[1].t + hit.v*hit.tri->m_vertices[2].t;
			const Image& img = *diffuseTex.getImage();
			Vec2i texelCoords = getTexelCoords(uv, img.getSize());
			//diffuse = img.getVec4f(texelCoords).getXYZ();
			Vec3f D = img.getVec4f(texelCoords).getXYZ();
			diffuse = Vec3f(pow(D.x,2.2f), pow(D.y, 2.2f), pow(D.z, 2.2f));		//gamma correction
		}
		else
		{
			diffuse = mat->diffuse.getXYZ();
		}
		if(specularsEnabled)
		{
			Texture& SpecularTex = mat->textures[MeshBase::TextureType_Specular];
			if(SpecularTex.exists())
			{
				const Image& img = *SpecularTex.getImage();
				Vec2i texelCoords = getTexelCoords(uv, img.getSize());
				specular = img.getVec4f(texelCoords).getXYZ();
			}
			else
			{
				specular = mat->specular;
			}
		}
	}


PathTracerContext::PathTracerContext()
    : m_bForceExit(false),
      m_bResidual(false),
      m_scene(nullptr),
      m_rt(nullptr),
      m_light(nullptr),
      m_pass(0),
      m_bounces(0),
      m_destImage(0),
      m_camera(nullptr)
{
}

PathTracerContext::~PathTracerContext()
{
}


PathTraceRenderer::PathTraceRenderer()
{
    m_raysPerSecond = 0.0f;
}

PathTraceRenderer::~PathTraceRenderer()
{
    stop();
}


Vec3f PathTraceRenderer::CosDir(Vec3f N, Vec3f SurfaceN, float Far, Random& R)		//generates random dirs
{
	Vec3f Dir;
	{
		float x = 10;
		float y = 10;
		while(pow(x, 2)+pow(y, 2)>1)				//generate point
		{
			x = R.getF32(-1, 1);
			y = R.getF32(-1, 1);
		}
		float z = sqrt(1-pow(x, 2)-pow(y, 2));		//lift to sphere
		Vec3f D = Vec3f(x, y, z);
		Mat3f Rot = formBasis(N);			//rotation matrix
		Dir = Rot*D*Far;
		if(m_normalMapped)
		{
			if(Dir.dot(SurfaceN) < 0)			//make sure it doesnt go into the surface
			{
				Dir += -Dir.dot(SurfaceN)*SurfaceN*1.01f;	//angle it along the surface, probably skrews with the pdf
			}
		}
	}
	return Dir;
}

// This function traces a single path and returns the resulting color value that will get rendered on the image. 
// Filling in the blanks here is all you need to do this time around.
Vec3f PathTraceRenderer::tracePath(float image_x, float image_y, PathTracerContext& ctx, int samplerBase, Random& R, std::vector<PathVisualizationNode>& visualization, float Dofx, float Dofy, bool DOF, bool BDPT)
{
	const MeshWithColors* scene = ctx.m_scene;
	RayTracer* rt = ctx.m_rt;
	Image* image = ctx.m_image.get();
	const CameraControls& cameraCtrl = *ctx.m_camera;
	AreaLight* light = ctx.m_light;

	std::vector<Node*> ToCheck;
	ToCheck.reserve(rt->BVH.size());

	// make sure we're on CPU
	//image->getMutablePtr();

	// get camera orientation and projection
	Mat4f worldToCamera = cameraCtrl.getWorldToCamera();
	Mat4f projection = Mat4f::fitToView(Vec2f(-1, -1), Vec2f(2, 2), image->getSize())*cameraCtrl.getCameraToClip();

	// inverse projection from clip space to world space
	Mat4f invP = (projection * worldToCamera).inverted();


	//BDPT
	//std::vector<Vec3f> BDPT_PointsL;
	//std::vector<float> BDPT_PDFsL;
	//std::vector<Vec3f> BDPT_E;
	Vec3f BDPT_PointsL;
	float BDPT_PDFsL;
	Vec3f BDPT_E;
	Vec3f BDPT_N;
	if(BDPT)
	{
		//if(debugVis)
		//	float asdasd = 1;
		//fire ray from light
		float PDF_L;						//get position
		Vec3f LightPos;
		light->sample(PDF_L, LightPos, 0, R);

		Vec3f LNorm = light->getNormal();	//fire ray
		Vec3f LDir = CosDir(LNorm, LNorm, cameraCtrl.getFar(), R);
		//Vec3f LDir = LNorm * cameraCtrl.getFar();

		RaycastResult Lresult = rt->raycast(LightPos, LDir, ToCheck);
		if(Lresult.tri  != nullptr)			//hit
		{
			Vec3f LPoint = Lresult.point;
			//BDPT_PointsL.push_back(LPoint);
			//direction-pdf = cos(theta)/PI
			//point-pdf = PDF_L
			float Dir_pdf = LNorm.dot(LDir.normalized())/FW_PI;		//cos1(theta)/PI		canceled by other calculations
			float P = PDF_L;
			//BDPT_PDFsL.push_back(P);
			
			Vec3f DiffuseTex;
			Vec3f SmoothNormal;
			Vec3f Specular;
			getTextureParameters(Lresult, DiffuseTex, SmoothNormal, Specular);
			if(SmoothNormal.dot(Lresult.dir) > 0)		//flip normal if wrong way
				SmoothNormal = -SmoothNormal;

			float r = (Lresult.t*LDir).length();
			Vec3f LEmission = light->getEmission();
			//float cos1  = LDir.dot(LNorm)/(LNorm.length() * r);	//xy and light normal		part of the pdf of direction
			float cos2  = -LDir.dot(SmoothNormal)/(SmoothNormal.length() * r);				//incoming and surface normal
			//if(cos1 < 0)
			//	cos1 = 0;
			if(cos2 < 0)
				cos2 = 0;

			//cos2 = 1;

			float InvSquare = 1.0f/(r*r);
			if(InvSquare > 10)
				InvSquare = 10;
			//Vec3f E = ((cos1 * cos2)/(r*r * P))*LEmission*DiffuseTex / FW_PI;
			Vec3f E = ((cos2*InvSquare)/(P))*LEmission*DiffuseTex / FW_PI;
			//Vec3f E = ((cos2)/(r*r * P))*LEmission*DiffuseTex;
			//Vec3f E = ((cos2)/(r*r))*LEmission*DiffuseTex;
			//Vec3f E = (1/(r*r * P))*LEmission*DiffuseTex;
			////BDPT_E.push_back(E);
			BDPT_PointsL=LPoint;
			BDPT_PDFsL = P;
			BDPT_E=E;
			BDPT_N=SmoothNormal;
		}

		if(debugVis)
		{
			PathVisualizationNode node;
			node.lines.push_back(PathVisualizationLine(LightPos, Lresult.point, Vec3f(1,1,0))); // Draws a line between two points
			//node.labels.push_back(PathVisualizationLabel("LIGHT", Lresult.point)); // You can also render text labels with world-space locations.
			visualization.push_back(node);
		}
	}



	// Simple ray generation code, you can use this if you want to.

	// Generate a ray through the pixel.
	float x,y;
	Vec4f P0, P1;
	x = (float)image_x / image->getSize().x *  2.0f - 1.0f;
	y = (float)image_y / image->getSize().y * -2.0f + 1.0f;

	//if(!DOF)			//not using DOF
	//{
		// point on front plane in homogeneous coordinates
		P0 = Vec4f(x, y, 0.0f, 1.0f);
		// point on back plane in homogeneous coordinates, same
		P1 = Vec4f(x, y, 1.0f, 1.0f);
	//}
	//else
	//{
	//	//float Dx = (float)Dofx / image->getSize().x *  2.0f - 1.0f;
	//	//float Dy = (float)Dofy / image->getSize().y * -2.0f + 1.0f;
	//	//// point on front plane in homogeneous coordinates, offset slightly
	//	//P0 = Vec4f(Dx, Dy, 0.0f, 1.0f);
	//	//// point on focal plane
	//	//Vec3f Pf = Vec3f(x, y, DofPlaneDist);
	//	//Pf = Pf - P0.getXYZ();
	//	//Pf = Pf*1.0f/Pf.z +P0.getXYZ();
	//	//// point on back plane in homogeneous coordinates
	//	//P1 = Vec4f(Pf,1.0f);

	//	// point on front plane in homogeneous coordinates, offset slightly
	//	P0 = Vec4f(x+Dofx, y+Dofy, 0.0f, 1.0f);
	//	//P0 = Vec4f(x, y, 0.0f, 1.0f);
	//	// point on focal plane
	//	//Vec3f Pf = Vec3f(x, y, DofPlaneDist);
	//	//Pf = Pf - P0.getXYZ();
	//	//Pf = Pf*1.0f/Pf.z +P0.getXYZ();

	//	//// point on back plane in homogeneous coordinates
	//	//P1 = Vec4f(Pf, 1.0f);

	//	P1 = Vec4f(x+Dofx-Dofx*1.0f/DofPlaneDist, y+Dofy-Dofy*1.0f/DofPlaneDist, 1.0f, 1.0f);

	//}


	// apply inverse projection, divide by w to get object-space points
	Vec4f Roh = (invP * P0);
	Vec3f Ro = (Roh * (1.0f / Roh.w)).getXYZ();
	Vec4f Rdh = (invP * P1);
	Vec3f Rd = (Rdh * (1.0f / Rdh.w)).getXYZ();

	// Subtract front plane point from back plane point,
	// yields ray direction.
	// NOTE that it's not normalized; the direction Rd is defined
	// so that the segment to be traced is [Ro, Ro+Rd], i.e.,
	// intersections that come _after_ the point Ro+Rd are to be discarded.




	Rd = Rd - Ro;
	if(DOF)
	{
		Vec3f Pf =Ro + DofPlaneDist*Rd;		//focal point

		Ro+= Vec3f(Dofx, Dofy, 0);			//offset origin
		
		Pf = Pf - Ro;						//direction from offset to focal point

		//Rd = Pf* 1.0f/DofPlaneDist;			//direction to end point
		Rd = Pf* Rd.length()/Pf.length();			//direction to end point
	}


	Vec3f CameraPos = Ro;

	// trace!
	RaycastResult result = rt->raycast(Ro, Rd, ToCheck);
	//const RTTriangle* pHit = result.tri;

	// if we hit something, fetch a color and insert into image
	Vec3f Ei(0,0,0);
	Vec3f throughput(1, 1, 1);
	//float p = 1.0f;
	int Bounce = 1;
	float Prob = 1;
	bool end = false;
	while(true)
	{
		if (result.tri != nullptr)
		{
			// YOUR CODE HERE (R2-R4):
			// Implement path tracing with direct light and shadows, scattering and Russian roulette.
			//if(debugVis)
			//	float asdasd = 1;
			Vec3f DiffuseTex;
			Vec3f SmoothNormal;
			Vec3f Specular;
			Vec3f Point = result.point;
			Vec3f Dir;
			getTextureParameters(result, DiffuseTex, SmoothNormal, Specular);
			if(SmoothNormal.dot(result.dir) > 0)		//flip normal if wrong way
				SmoothNormal = -SmoothNormal;

			float pdf;
			Vec3f LightPos;
			light->sample(pdf, LightPos, 0, R);
			Vec3f ShadowDir = LightPos - Point;
			//RaycastResult ShadowRes = rt->raycast(Point+ShadowDir*0.001f, ShadowDir, ToCheck);
			RaycastResult ShadowRes = rt->raycast(Point+SmoothNormal*0.001f, ShadowDir, ToCheck);
			
			if(Specular.length() != 0)		//if it is reflective, this is not correct since i use no pdf's etc
			{
				Vec3f V;								//toward incoming(camera)
				Vec3f L;								//toward outgoing(light)
				float q = result.tri->m_material->glossiness;
				//float MRand = R.getF32(0, 1);
				//if(MRand < 0.8)			//sample mirror direction
				{
					Vec3f Mirror = (result.dir -2*result.dir.dot(SmoothNormal)*SmoothNormal).normalized();
					Dir = (CosDir(Mirror, Mirror, 1, R) + Mirror*q/128).normalized()*cameraCtrl.getFar();	//next bounce close to mirror direction
					V = result.dir.normalized();			//toward incoming, actually from incoming, doesnt work if negative
					L = Dir.normalized();					//continuation ray
				}
				//else					//sample light, produce highlights, too noisy for my taste
				//{
				//	
				//	if((ShadowRes.tri == nullptr)&&(light->getNormal().dot(ShadowDir) < 0))				//isn't blocked, and not from behind
				//	{
				//		end = true;			//stop the bounces
				//		V = -result.dir.normalized();				//toward incoming
				//		L = (LightPos - Point).normalized();		//toward light
				//	}
				//	else
				//	{						//sample diffuse
				//		Dir = CosDir(SmoothNormal, result.tri->normal(), cameraCtrl.getFar(), R);			//next bounce diffuse distribution
				//		V = result.dir.normalized();			//toward incoming, actually from incoming, doesnt work if negative dont know why
				//		L = Dir.normalized();					//continuation ray
				//	}
				//}

				float CalcSpec;

				Vec3f H = (L+V)/(length(L+V));

				//float F = 1;
				float R0 = 0.04f; //glass
				//float R0 = pow((RefIndex - 1)/(RefIndex + 1), 2);		//between surface and air
				float F = R0 + (1- R0)*pow((1-SmoothNormal.dot(L)), 5);
				//float D = 1;
				float Nq = (q+1)/(2*FW_PI);
				float D = Nq*pow(SmoothNormal.dot(H), q);
				//float G = 1;
				float G = min(1.0f, 2.0f*(SmoothNormal.dot(H))*(SmoothNormal.dot(V))/V.dot(H), 2.0f*(SmoothNormal.dot(H))*(SmoothNormal.dot(L))/V.dot(H));

				CalcSpec = F*D*G /(SmoothNormal.dot(L)*SmoothNormal.dot(V));

				Specular *= CalcSpec;
			}
			else
			{
				Dir = CosDir(SmoothNormal, result.tri->normal(), cameraCtrl.getFar(), R);
			}

			//float materailpdf = cos(theta)/PI;
			throughput *= DiffuseTex + Specular; //  * cos(theta)/PI / materialpdf
			Vec3f ELR, EL;
			if(BDPT)
			{
				//Vec3f P_L = BDPT_PointsL[0];
				//float PDF_L = BDPT_PDFsL[0];
				//Vec3f E_L = BDPT_E[0];
				Vec3f P_L = BDPT_PointsL;
				float PDF_L = BDPT_PDFsL;
				Vec3f E_L = BDPT_E;
				Vec3f N_L = BDPT_N;

				Vec3f BDPTDir = P_L - Point;
				
				//RaycastResult BDPTres = rt->raycast(Point+BDPTDir*0.001f, BDPTDir*0.999f, ToCheck);		//slightly before the point otherwise get lots of noise
				RaycastResult BDPTres = rt->raycast(Point+SmoothNormal*0.001f, BDPTDir*0.999f+N_L*0.001f, ToCheck);		//slightly before the point otherwise get lots of noise
				if(BDPTres.tri == nullptr)
				{
					//float cos2 = N_L.dot(-BDPTDir.normalized());
					//Ei += E_L * throughput * cos2;					//add contribution

					float r = BDPTDir.length();
					float cos1  = -BDPTDir.dot(N_L)/(N_L.length() * r);						//xy and light normal
					float cos2  = BDPTDir.dot(SmoothNormal)/(SmoothNormal.length() * r);	//incoming and surface normal
					if(cos1 < 0)
						cos1 = 0;
					if(cos2 < 0)
						cos2 = 0;

					float InvSquare = 1.0f/(r*r);
					if(InvSquare > 10)
						InvSquare = 10;

					//Ei += (((cos1 * cos2)/(r*r*PDF_L))* E_L * Prob * throughput /FW_PI)/2;			//add contribution, half from this point half sampled from the light
					Ei += (((cos1 * cos2 * InvSquare)/(PDF_L))* E_L * Prob * throughput /FW_PI)/2;			//add contribution, half from this point half sampled from the light
					//ELR = (((cos1 * cos2)/(r*r * PDF_L))* E_L * Prob * throughput /FW_PI);			//add contribution
					//Ei += (((cos1 * cos2)/(r*r))* E_L * Prob * throughput /FW_PI);			//add contribution
					if(debugVis)
					{
						PathVisualizationNode node;
						node.lines.push_back(PathVisualizationLine(Point, Point + BDPTDir, Vec3f(0, 0, 1))); // blue line if it "hits"

						visualization.push_back(node);
					}
				}
				else if(debugVis)
				{
					PathVisualizationNode node;
					node.lines.push_back(PathVisualizationLine(Point, BDPTres.point, Vec3f(1, 0, 0))); // red line if it collides with something

					visualization.push_back(node);
				}
				
			}
			if(ShadowRes.tri == nullptr)						// trace shadow ray to see if it's blocked,	taken from assignment 2
			{
				// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
				// accumulate into E
				float r = ShadowDir.length();
				Vec3f LEmission = light->getEmission();
				Vec3f LNorm = light->getNormal();
				float cos1  = -ShadowDir.dot(LNorm)/(LNorm.length() * r);	//xy and light normal
				float cos2  = ShadowDir.dot(SmoothNormal)/(SmoothNormal.length() * r);				//incoming and surface normal
				if(cos1 < 0)
					cos1 = 0;
				if(cos2 < 0)
					cos2 = 0;

				if(BDPT)
				{
					Ei += (((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * throughput / FW_PI)/2;		//add contribution, half from light half from point cast from light
					//EL = (((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * throughput / FW_PI);		//add contribution, half from light half from point cast from light
				}
				else
				{
					Ei += ((cos1 * cos2)/(r*r * pdf))*LEmission * Prob * throughput / FW_PI;			//add contribution
				}
				
			}
			//if(BDPT)
			//{
			//	if(ELR.length() != 0)
			//	{
			//		if(EL.length() != 0)
			//			Ei += ELR/2 + EL/2;
			//		else
			//			Ei += ELR;
			//	}
			//	else if(EL.length() != 0)
			//	{
			//		Ei += EL;
			//	}

			//}
			if (debugVis)
			{
				// Example code for using the visualization system. You can expand this to include further bounces, 
				// shadow rays, and whatever other useful information you can think of.
				PathVisualizationNode node;
				node.lines.push_back(PathVisualizationLine(result.orig, result.point)); // Draws a line between two points
				//node.lines.push_back(PathVisualizationLine(result.point, result.point + result.tri->normal() * 30.0f, Vec3f(1, 0, 0))); // You can give lines a color as optional parameter.
				node.lines.push_back(PathVisualizationLine(result.point, result.point + SmoothNormal		 * 1.0f, Vec3f(0, 1, 0)));
				node.labels.push_back(PathVisualizationLabel("diffuse: " + std::to_string(Ei.x) + ", " + std::to_string(Ei.y) + ", " + std::to_string(Ei.z), result.point)); // You can also render text labels with world-space locations.

				visualization.push_back(node);
			}
			//if(end)
			//	break;
			if(ctx.m_bounces < 0)		
			{
				if(Bounce <= -ctx.m_bounces)		//russion roulette
				{
					Bounce++;						//continue
					result = rt->raycast(Point+Dir*0.001f/cameraCtrl.getFar(), Dir, ToCheck);
				}
				else
				{
					float Rand = R.getF32(0, 1);
					if(Rand < 0.2)
					{
						Bounce++;					//continue
						Prob *= 1/0.8;
						result = rt->raycast(Point+Dir*0.001f/cameraCtrl.getFar(), Dir, ToCheck);
					}
					else
					{
						break;			//terminate
					}
				}
			}
			else
			{
				if(Bounce <= ctx.m_bounces)		//normal
				{
					Bounce++;					//continue
					result = rt->raycast(Point+Dir*0.001f/cameraCtrl.getFar(), Dir, ToCheck);
				}
				else
				{
					break;			//terminate
				}
			}
		}
		else
		{
			break;		//missed
		}
	}
	return Ei;
}

// This function is responsible for asynchronously generating paths for a given block.
void PathTraceRenderer::pathTraceBlock( MulticoreLauncher::Task& t )
{
    PathTracerContext& ctx = *(PathTracerContext*)t.data;

    const MeshWithColors* scene			= ctx.m_scene;
    RayTracer* rt						= ctx.m_rt;
    Image* image						= ctx.m_image.get();
    const CameraControls& cameraCtrl	= *ctx.m_camera;
    AreaLight* light					= ctx.m_light;

    // make sure we're on CPU
    image->getMutablePtr();

    // get camera orientation and projection
    Mat4f worldToCamera = cameraCtrl.getWorldToCamera();
    Mat4f projection = Mat4f::fitToView(Vec2f(-1,-1), Vec2f(2,2), image->getSize())*cameraCtrl.getCameraToClip();

    // inverse projection from clip space to world space
    Mat4f invP = (projection * worldToCamera).inverted();

    // get the block which we are rendering
    PathTracerBlock& block = ctx.m_blocks[t.idx];

	// Not used but must be passed to tracePath
	std::vector<PathVisualizationNode> dummyVisualization; 

	static std::atomic<uint32_t> seed = 0;
	uint32_t current_seed = seed.fetch_add(1);
	Random R(t.idx + current_seed);	// this is bogus, just to make the random numbers change each iteration

    for ( int i = 0; i < block.m_width * block.m_height; ++i )
    {
        if( ctx.m_bForceExit ) {
            return;
        }

        // Use if you want.
        int pixel_x = block.m_x + (i % block.m_width);
        int pixel_y = block.m_y + (i / block.m_width);

		float Rpixel_x = pixel_x + R.getF32(-0.5f, 0.5f);		//randomized
		float Rpixel_y = pixel_y + R.getF32(-0.5f, 0.5f);

		Vec3f Ei;
		if(DofOffset != 0)			//if using DOF
		{
			//Vec3f Ei1 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, Rpixel_x - DofOffset, Rpixel_y - DofOffset,true);
			//Vec3f Ei2 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, Rpixel_x + DofOffset, Rpixel_y - DofOffset,true);
			//Vec3f Ei3 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, Rpixel_x + DofOffset, Rpixel_y + DofOffset,true);
			//Vec3f Ei4 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, Rpixel_x - DofOffset, Rpixel_y + DofOffset,true);

			float ROffset1 = R.getF32(-DofOffset, DofOffset);
			float ROffset2 = R.getF32(-DofOffset, DofOffset);

			Ei = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei1 = tracePath(Rpixel_x-1, Rpixel_y-1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei2 = tracePath(Rpixel_x  , Rpixel_y-1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei3 = tracePath(Rpixel_x+1, Rpixel_y-1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);

			//Vec3f Ei4 = tracePath(Rpixel_x-1, Rpixel_y  , ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei5 = tracePath(Rpixel_x  , Rpixel_y  , ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei6 = tracePath(Rpixel_x+1, Rpixel_y  , ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);

			//Vec3f Ei7 = tracePath(Rpixel_x-1, Rpixel_y+1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei8 = tracePath(Rpixel_x  , Rpixel_y+1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei9 = tracePath(Rpixel_x+1, Rpixel_y+1, ctx, 0, R, dummyVisualization, ROffset1, ROffset2, true, BDPTEnabled);
			//Vec3f Ei5 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, 0, 0, false);
			//Vec3f Ei1 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, -DofOffset, -DofOffset, true);
			//Vec3f Ei2 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, +DofOffset, -DofOffset, true);
			//Vec3f Ei3 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, +DofOffset, +DofOffset, true);
			//Vec3f Ei4 = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization, -DofOffset, +DofOffset, true);

			//Ei = (Ei1 + Ei2 + Ei3 + Ei4 + Ei5 + Ei6 + Ei7 + Ei8 + Ei9)/9.0f;
			//Ei = Ei1;
		}
		else
		{							//standard
			Ei = tracePath(Rpixel_x, Rpixel_y, ctx, 0, R, dummyVisualization,0,0,false,BDPTEnabled);

			//Vec3f Ei1 = tracePath(Rpixel_x-1, Rpixel_y-1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei2 = tracePath(Rpixel_x  , Rpixel_y-1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei3 = tracePath(Rpixel_x+1, Rpixel_y-1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);

			//Vec3f Ei4 = tracePath(Rpixel_x-1, Rpixel_y, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei5 = tracePath(Rpixel_x  , Rpixel_y, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei6 = tracePath(Rpixel_x+1, Rpixel_y, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);

			//Vec3f Ei7 = tracePath(Rpixel_x-1, Rpixel_y+1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei8 = tracePath(Rpixel_x  , Rpixel_y+1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Vec3f Ei9 = tracePath(Rpixel_x+1, Rpixel_y+1, ctx, 0, R, dummyVisualization, 0, 0, true, BDPTEnabled);
			//Ei = (Ei1 + Ei2 + Ei3 + Ei4 + Ei5 + Ei6 + Ei7 + Ei8 + Ei9)/9.0f;
		}
        // Put pixel.
        Vec4f prev = image->getVec4f( Vec2i(pixel_x, pixel_y) );
        prev += Vec4f( Ei, 1.0f );
        image->setVec4f( Vec2i(pixel_x, pixel_y), prev );
    }
}

void PathTraceRenderer::startPathTracingProcess( const MeshWithColors* scene, AreaLight* light, RayTracer* rt, Image* dest, int bounces, const CameraControls& camera )
{
    FW_ASSERT( !m_context.m_bForceExit );

    m_context.m_bForceExit = false;
    m_context.m_bResidual = false;
    m_context.m_camera = &camera;
    m_context.m_rt = rt;
    m_context.m_scene = scene;
    m_context.m_light = light;
    m_context.m_pass = 0;
    m_context.m_bounces = bounces;
    m_context.m_image.reset(new Image( dest->getSize(), ImageFormat::RGBA_Vec4f));

    m_context.m_destImage = dest;
    m_context.m_image->clear();

    // Add rendering blocks.
    m_context.m_blocks.clear();
    {
        int block_size = 32;
        int image_width = dest->getSize().x;
        int image_height = dest->getSize().y;
        int block_count_x = (image_width + block_size - 1) / block_size;
        int block_count_y = (image_height + block_size - 1) / block_size;

        for(int y = 0; y < block_count_y; ++y) {
            int block_start_y = y * block_size;
            int block_end_y = FW::min(block_start_y + block_size, image_height);
            int block_height = block_end_y - block_start_y;

            for(int x = 0; x < block_count_x; ++x) {
                int block_start_x = x * block_size;
                int block_end_x = FW::min(block_start_x + block_size, image_width);
                int block_width = block_end_x - block_start_x;

                PathTracerBlock block;
                block.m_x = block_size * x;
                block.m_y = block_size * y;
                block.m_width = block_width;
                block.m_height = block_height;

                m_context.m_blocks.push_back(block);
            }
        }
    }

    dest->clear();

    // Fire away!

    // If you change this, change the one in checkFinish too.

#ifdef _DEBUG
	m_launcher.setNumThreads(1);
#else
	m_launcher.setNumThreads(m_launcher.getNumCores());
#endif

    m_launcher.popAll();
    m_launcher.push( pathTraceBlock, &m_context, 0, (int)m_context.m_blocks.size() );
}

void PathTraceRenderer::updatePicture( Image* dest )
{
    FW_ASSERT( m_context.m_image != 0 );
    FW_ASSERT( m_context.m_image->getSize() == dest->getSize() );

    for ( int i = 0; i < dest->getSize().y; ++i )
    {
        for ( int j = 0; j < dest->getSize().x; ++j )
        {
            Vec4f D = m_context.m_image->getVec4f(Vec2i(j,i));
            if ( D.w != 0.0f )
                D = D*(1.0f/D.w);

            // Gamma correction.
            Vec4f color = Vec4f(
                FW::pow(D.x, 1.0f / 2.2f),
                FW::pow(D.y, 1.0f / 2.2f),
                FW::pow(D.z, 1.0f / 2.2f),
                D.w
            );

            dest->setVec4f( Vec2i(j,i), color );
        }
    }
}

void PathTraceRenderer::checkFinish()
{
    // have all the vertices from current bounce finished computing?
    if ( m_launcher.getNumTasks() == m_launcher.getNumFinished() )
    {
        // yes, remove from task list
        m_launcher.popAll();

        ++m_context.m_pass;

        // you may want to uncomment this to write out a sequence of PNG images
        // after the completion of each full round through the image.
        //String fn = sprintf( "pt-%03dppp.png", m_context.m_pass );
        //File outfile( fn, File::Create );
        //exportLodePngImage( outfile, m_context.m_destImage );

        if ( !m_context.m_bForceExit )
        {
            // keep going

            // If you change this, change the one in startPathTracingProcess too.
#ifdef _DEBUG
			m_launcher.setNumThreads(1);
#else
			m_launcher.setNumThreads(m_launcher.getNumCores());
#endif

            m_launcher.popAll();
            m_launcher.push( pathTraceBlock, &m_context, 0, (int)m_context.m_blocks.size() );
            //::printf( "Next pass!" );
        }
        else ::printf( "Stopped." );
    }
}

void PathTraceRenderer::stop() {
    m_context.m_bForceExit = true;
    
    if ( isRunning() )
    {
        m_context.m_bForceExit = true;
        while( m_launcher.getNumTasks() > m_launcher.getNumFinished() )
        {
            Sleep( 1 );
        }
        m_launcher.popAll();
    }

    m_context.m_bForceExit = false;
}



} // namespace FW
