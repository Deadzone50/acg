#define _CRT_SECURE_NO_WARNINGS

#include "base/Defs.hpp"
#include "base/Math.hpp"
#include "RayTracer.hpp"
#include <stdio.h>
#include "rtIntersect.inl"
#include <fstream>

#include "rtlib.hpp"
#include <memory>
#include <algorithm>    // std::sort
#include <vector>
#include <stack>
#include <math.h>
#include "base/Random.hpp"

// Helper function for hashing scene data for caching BVHs
extern "C" void MD5Buffer( void* buffer, size_t bufLen, unsigned int* pDigest );


namespace FW
{


Vec2f getTexelCoords(Vec2f uv, const Vec2i size)
{

	// YOUR CODE HERE (R1):
	// Integrate your implementation here.
	Vec2f UVi = (Vec2i)uv;
	Vec2f Wrap = uv-(Vec2f)UVi;

	Wrap = Wrap + 1;
	Vec2f Wrapi = (Vec2i)Wrap;
	Vec2f Wrap2 = Wrap-(Vec2f)Wrapi;

	Vec2f Map = Wrap2*(Vec2f)size;

	return Map;
}

Mat3f formBasis(const Vec3f& n) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	Vec3f Q = n;
	if(abs(Q.x) <= abs(Q.y) && abs(Q.x) <= abs(Q.z))	//find smallest, replace with 1
		Q.x = 1;
	else if(abs(Q.y) <= abs(Q.x) && abs(Q.y) <= abs(Q.z))
		Q.y = 1;
	else
		Q.z = 1;

	Vec3f T = Q.cross(n).normalized();
	Vec3f B = n.cross(T);

	Mat3f R;

	R.setCol(0, T);
	R.setCol(1, B);
	R.setCol(2, n);
	return R;
    //return rtlib::formBasis(n);
}


String RayTracer::computeMD5( const std::vector<Vec3f>& vertices )
{
    unsigned char digest[16];
    MD5Buffer( (void*)&vertices[0], sizeof(Vec3f)*vertices.size(), (unsigned int*)digest );

    // turn into string
    char ad[33];
    for ( int i = 0; i < 16; ++i )
        ::sprintf( ad+i*2, "%02x", digest[i] );
    ad[32] = 0;

    return FW::String( ad );
}


// --------------------------------------------------------------------------


RayTracer::RayTracer()
{
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
    // After that this is not needed anymore.
    //m_rt.reset(new rtlib::RayTracer);
}

RayTracer::~RayTracer()
{
	for(auto it = BVH.begin(); it != BVH.end(); ++it)		//delete the reserved memory for the nodes
	{
		delete *it;
	}
	BVH.clear();
}
// Read a simple data type from a stream.
template<class T>
std::istream& read(std::istream& os, T& x)
{
	return os.read(reinterpret_cast<char*>(&x), sizeof(x));
}

void ReadNode(std::ifstream &file, Node* N)
{
	read(file, N->AABB[0].x);
	read(file, N->AABB[0].y);
	read(file, N->AABB[0].z);

	read(file, N->AABB[1].x);
	read(file, N->AABB[1].y);
	read(file, N->AABB[1].z);

	read(file, N->I1);
	read(file, N->I2);
}

void RayTracer::loadHierarchy(const char* filename, std::vector<RTTriangle>& triangles)
{
    // YOUR CODE HERE (R1):
    // Integrate your implementation here

	// YOUR CODE HERE (R2):
	size_t TrigIsize, BVHsize;
	bool Children;
	std::ifstream file(filename, std::ios::binary);
	read(file, TrigIsize);
	TrigI.resize(TrigIsize);
	for(int i = 0; i < TrigIsize; ++i)
	{
		read(file, TrigI[i]);
	}
	read(file, BVHsize);

	Node* N = new Node;
	Node* LC;
	Node* RC;
	ReadNode(file, N);
	std::deque<Node*> ToRead;
	ToRead.push_back(N);
	BVH.push_back(N);

	while(ToRead.size() != 0)
	{
		N = ToRead.front();
		ToRead.pop_front();

		read(file, Children);		//if it has children

		if(Children)
		{
			LC = new Node;
			ReadNode(file, LC);
			ToRead.push_back(LC);
			//BVH.push_back(LC);
			N->LeftChild = LC;

			RC = new Node;
			ReadNode(file, RC);
			ToRead.push_back(RC);
			//BVH.push_back(RC);
			N->RightChild = RC;
		}
		//else
		//{
		//	N->LeftChild = NULL;
		//	N->RightChild = NULL;
		//}
	}
	file.close();
	m_triangles = &triangles;

	//m_rt->loadHierarchy(filename, triangles);
	//m_triangles = &triangles;
}
// Write a simple data type to a stream.
template<class T>
std::ostream& write(std::ostream& stream, const T& x)
{
	return stream.write(reinterpret_cast<const char*>(&x), sizeof(x));
}

void WriteNode(std::ofstream &file, Node* N)
{
	write(file, N->AABB[0].x);
	write(file, N->AABB[0].y);
	write(file, N->AABB[0].z);

	write(file, N->AABB[1].x);
	write(file, N->AABB[1].y);
	write(file, N->AABB[1].z);

	write(file, N->I1);
	write(file, N->I2);
}

void RayTracer::saveHierarchy(const char* filename, const std::vector<RTTriangle>& triangles) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
    
	std::ofstream file(filename, std::ios::binary);
	write(file, TrigI.size());
	for(int i = 0; i < TrigI.size(); ++i)
	{
		write(file, TrigI[i]);
	}
	write(file, BVH.size());

	Node* N = BVH[0];
	WriteNode(file, N);
	std::deque<Node*> ToWrite;
	ToWrite.push_back(N);

	while(ToWrite.size() != 0)
	{
		N = ToWrite.front();
		ToWrite.pop_front();
		if(N->LeftChild != NULL)
		{
			write(file, (bool)1);		//if it has children
			WriteNode(file, N->LeftChild);
			ToWrite.push_back(N->LeftChild);

			WriteNode(file, N->RightChild);
			ToWrite.push_back(N->RightChild);
		}
		else
			write(file, (bool)0);
	}
	file.close();
	
	//m_rt->saveHierarchy(filename, triangles);
}

float CheckCost(size_t I1, size_t I2, std::vector<int>& TrigI, int& Best, std::vector<RTTriangle>& Trigs)
{
	Vec3f Min1 = FLT_MAX;
	Vec3f Max1 = -FLT_MAX;
	Vec3f Min2 = FLT_MAX;
	Vec3f Max2 = -FLT_MAX;

	float Cost = FLT_MAX;
	size_t Optimal = 0;
	std::vector<float> LSA(I2-I1);			//contain sizes of aabbs
	std::vector<float> RSA(I2-I1);

	for(int i = 0; i < I2-I1; ++i)
	{
		auto &T = Trigs[TrigI[i+I1]];
		Vec3f Val = T.min();
		if(Val.x < Min1.x)
			Min1.x = Val.x;
		if(Val.y < Min1.y)
			Min1.y = Val.y;
		if(Val.z < Min1.z)
			Min1.z = Val.z;

		Val = T.max();
		if(Val.x > Max1.x)
			Max1.x = Val.x;
		if(Val.y > Max1.y)
			Max1.y = Val.y;
		if(Val.z > Max1.z)
			Max1.z = Val.z;

		LSA[i] = (Max1.x - Min1.x)*(Max1.y - Min1.y)*2  + (Max1.x - Min1.x)*(Max1.z - Min1.z)*2 + (Max1.y - Min1.y)*(Max1.z - Min1.z)*2;		//surface area of aabb
	}
	for(int i = I2-I1-1; i >= 0; --i)
	{
		auto &T = Trigs[TrigI[i+I1]];
		Vec3f Val = T.min();
		if(Val.x < Min2.x)
			Min2.x = Val.x;
		if(Val.y < Min2.y)
			Min2.y = Val.y;
		if(Val.z < Min2.z)
			Min2.z = Val.z;

		Val = T.max();
		if(Val.x > Max2.x)
			Max2.x = Val.x;
		if(Val.y > Max2.y)
			Max2.y = Val.y;
		if(Val.z > Max2.z)
			Max2.z = Val.z;

		RSA[i] = (Max2.x - Min2.x)*(Max2.y - Min2.y)*2  + (Max2.x - Min2.x)*(Max2.z - Min2.z)*2 + (Max2.y - Min2.y)*(Max2.z - Min2.z)*2;		//surface area of aabb
	}

	for(int i = 1; i < I2-I1-1; ++i)				//dont allow splits with 0 elements in one side
	{
		float C = LSA[i] * i + RSA[i]*(I2-I1-i);
		if(C < Cost)
		{
			Cost = C;
			Optimal = i+I1;
		}
	}
	Best = Optimal;
	return Cost;
}

void RayTracer::constructHierarchy(std::vector<RTTriangle>& triangles, SplitMode splitMode) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.

	Vec3f Min = FLT_MAX;
	Vec3f Max = -FLT_MAX;
	Node *N;

	if(TrigI.size() == 0)			//create indexlist if it does not exsist
	{
		for(int i = 0; i< triangles.size(); ++i)
		{
			TrigI.push_back(i);
			TrigIx.push_back(i);
			TrigIy.push_back(i);
			TrigIz.push_back(i);
		}
	}
	if(BVH.size() == 0)				//root node
	{
		N = new Node;
		N->I1 = 0;
		N->I2 = triangles.size();
		BVH.push_back(N);
	}
	else
	{
		N = BVH.back();
	}
	size_t i = N->I1;
	while(i != N->I2)				//check for longest dimensions
	{
		auto &T = triangles[TrigI[i]];
		Vec3f Val = T.min();
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		Val = T.max();
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;
		++i;
	}
	N->AABB[0] = Min;					//AABB
	N->AABB[1] = Max;

	if(N->I2 - N->I1 < 5)				//stop if there are few triangles left
	{
		N->LeftChild = NULL;
		N->RightChild = NULL;
		//std::cout << "leaf\n";
		return;
	}

	int BestX;
	int BestY;
	int BestZ;
	//sort objects based on their centerpoint along all axes
	std::sort(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().x < triangles[i2].centroid().x; });	//up to not including I2
	//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().x < triangles[i2].min().x; });	//up to not including I2
	float CostX = CheckCost(N->I1, N->I2, TrigIx, BestX, triangles);

	std::sort(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().y < triangles[i2].centroid().y; });
	//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().y < triangles[i2].min().y; });
	float CostY = CheckCost(N->I1, N->I2, TrigIy, BestY, triangles);

	std::sort(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().z < triangles[i2].centroid().z; });
	//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].min().z < triangles[i2].min().z; });
	float CostZ = CheckCost(N->I1, N->I2, TrigIz, BestZ, triangles);

	int Split;
	if(CostX <= CostY)
	{
		if(CostX <= CostZ)	//X is the best
		{
			Split = BestX;
			//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().x < triangles[i2].centroid().x; });	//up to not including I2
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigIy.begin() + N->I1);
			std::copy(TrigIx.begin() + N->I1, TrigIx.begin()+ N->I2, TrigIz.begin() + N->I1);
		}
		else				//Z is the best
		{
			Split = BestZ;
			//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().z < triangles[i2].centroid().z; });	//up to not including I2
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIy.begin() + N->I1);
		}
	}
	else
	{
		if(CostY <= CostZ)	//Y is the best
		{
			Split = BestY;
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIy.begin() + N->I1, TrigIy.begin()+ N->I2, TrigIz.begin() + N->I1);
			//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().y < triangles[i2].centroid().y; });	//up to not including I2
		}
		else
		{					//Z is the best
			Split = BestZ;
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigI.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIx.begin() + N->I1);
			std::copy(TrigIz.begin() + N->I1, TrigIz.begin()+ N->I2, TrigIy.begin() + N->I1);
			//std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().z < triangles[i2].centroid().z; });	//up to not including I2
		}
	}


	N->LeftChild = new Node;
	N->LeftChild->I1 = N->I1;
	N->LeftChild->I2 = Split;
	BVH.push_back(N->LeftChild);
	constructHierarchy(triangles, splitMode);	//recursion

	N->RightChild = new Node;
	N->RightChild->I1 = Split;
	N->RightChild->I2 = N->I2;
	BVH.push_back(N->RightChild);
	constructHierarchy(triangles, splitMode);	//recursion

	m_triangles = &triangles;

	//m_rt->constructHierarchy(triangles, splitMode);
	//m_triangles = &triangles;
}
struct Hit
{
	bool H;
	float T1;
	float T2;
};

Hit
IntersectsNode(const Node& N, const Vec3f& orig, const Vec3f& InvDir, Vec3i& Swap, float TMax)		//test if ray hits a node
{
	float T1X = (N.AABB[Swap[0]].x - orig.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T2X = (N.AABB[1-Swap[0]].x - orig.x)*InvDir.x;

	float T1Y = (N.AABB[Swap[1]].y - orig.y)*InvDir.y;		//intersection y
	float T2Y = (N.AABB[1-Swap[1]].y - orig.y)*InvDir.y;

	if(T1X < T1Y)				//max of mins
		T1X = T1Y;
	if(T2X > T2Y)				//min of maxs
		T2X = T2Y;

	float T1Z = (N.AABB[Swap[2]].z - orig.z)*InvDir.z;		//intersection z
	float T2Z = (N.AABB[1-Swap[2]].z - orig.z)*InvDir.z;

	if(T1X < T1Z)				//max of mins
		T1X = T1Z;
	if(T2X > T2Z)				//min of maxs
		T2X = T2Z;

	if(T1X > TMax)				//check that it isn't too far
		return Hit{false};
	if(T1X > T2X)				//miss
	{
		return Hit{false};
	}
	else if(T2X < 0)			//behind, miss
	{
		return Hit{false};
	}
	else if(T1X > 0)			//hit
	{
		return Hit{true, T1X, T2X};
	}
	else
	{							//origin inside box
		return Hit{true, 0, T2X};
	}
}

//RaycastResult RayTracer::raycast(const Vec3f& orig, const Vec3f& dir) const 
RaycastResult RayTracer::raycast(const Vec3f& orig, const Vec3f& dir, std::vector<Node*> &ToCheck) const
{
	++m_rayCount;

	//YOUR CODE HERE (R1):
	//This is where you hierarchically traverse the tree you built!
	//You can use the existing code for the leaf nodes.

	//int Trig = 0;				//used for checking efficency of BVH
	//int Thit = 0;
	//int Lhit = 0;
	//int Rhit = 0;
	//int Bhit = 0;

	Hit H, HL, HR;
	int imin = -1;
	float tmin = 1.0f, umin = 0.0f, vmin = 0.0f;

	Vec3f InvDir;					//precompute inverse of direction
	InvDir[0] = 1.0f/dir[0];		//floating point division by zero defined
	InvDir[1] = 1.0f/dir[1];
	InvDir[2] = 1.0f/dir[2];

	Vec3i Swap;						//precalculate if the bounds have to be swapped
	Swap.x = (dir.x < 0);
	Swap.y = (dir.y < 0);
	Swap.z = (dir.z < 0);

	H = IntersectsNode(*BVH[0], orig, InvDir, Swap, tmin);
	if(H.H)							//check if hit root node
	{
		//std::vector<Node*> ToCheck;
		//ToCheck.reserve(BVH.size());
		ToCheck.push_back(BVH[0]);
		while(ToCheck.size() != 0)
		{
			Node* N = ToCheck.back();
			ToCheck.pop_back();
			if(N->LeftChild != NULL)	//check if leafnode
			{
				HL = IntersectsNode(*N->LeftChild, orig, InvDir, Swap, tmin);
				HR = IntersectsNode(*N->RightChild, orig, InvDir, Swap, tmin);

				//if(HL.T1 > tmin)			//if the minimum of the AABB is farther away than the closest found, discard
				//	HL.H = false;
				//if(HR.T1 > tmin)
				//	HR.H = false;

				if(HL.H && HR.H)			//if both hit, check closest first
				{
					//Bhit++;
					if(HL.T1 < HR.T1)
					{
						ToCheck.push_back(N->RightChild);
						ToCheck.push_back(N->LeftChild);
					}
					else
					{
						ToCheck.push_back(N->LeftChild);
						ToCheck.push_back(N->RightChild);
					}
				}
				else if(HL.H)
				{
					//Lhit++;
					ToCheck.push_back(N->LeftChild);
				}
				else if(HR.H)
				{
					//Rhit++;
					ToCheck.push_back(N->RightChild);
				}
				else
				{
					//miss
				}
			}
			else
			{		//leafnode, check triangles
				for(size_t i = N->I1; i < N->I2; ++i)
				{
					//++Trig;		//count number of triangle tested per ray
					float t, u, v;
					if((*m_triangles)[TrigI[i]].intersect_woop(orig, dir, t, u, v))
					{
						if(t > 0.0f && t < tmin)
						{
							imin = (int)i;
							tmin = t;
							umin = u;
							vmin = v;
						}
					}
				}
			}
		}
	}
	//std::cout << "Triangles read:" << Trig << std::endl;
	//Thit = Bhit+Lhit+Rhit;
	//int tot = BVH.size();
	//std::cout << "Hits B, L, R, T:" << Bhit << ", " << Lhit << ", " << Rhit << ", " << Thit << ", " << tot << std::endl;
	RaycastResult castresult;
	if(imin != -1)
	{
		castresult = RaycastResult(&(*m_triangles)[TrigI[imin]], tmin, umin, vmin, orig + tmin*dir, orig, dir);
	}
	return castresult;

    //return m_rt->raycast(orig, dir);
}


} // namespace FW