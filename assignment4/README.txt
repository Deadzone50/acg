# ME-E4100 Advanced Computer Graphics, Spring 2016
# Lehtinen / Kemppinen, Ollikainen
#
# Assignment 4: Path Tracing

Student name: Viktor M�sala
Student number: 84732N

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1 Integrate your raytracer (1p): done
  R2 Direct light & shadows (2p): done
  R3 Bounced indirect light (5p): done
        R4 Russian roulette (2p): done

Remember the other mandatory deliverables!
- 6 images rendered with our default presets
- Rendering competition PDF + images in separate folder

# Did you do any extra credit work?
-Tangent space normal mapping
-Surface area heuristics, i have not done it in any earlier assignements.
-Specular reflections, no highlights/light reflections since i couldn't figure out how to do them without alot of noise. the commented out code produces the same effect as the reference assignment, but it also has alot of noise(bright dots on other surfaces, very noticable if floor of crytek sponza is set to reflect light, the colored cloth curtains will have alot of bright dots on them).
-Tried to make a Depth of field effect, but i didn't get it to work properly it mostly makes a blurry mess out of everything, can produce a Dof effect in some circumstances. 2 new sliders to controll it, gets enabled when Dof offset is other than 0.
-Bi-directional path tracing, tried to implement it with one bounce from the light, but it is not quite right. in some circumstances the image is way to bright, but most of the time it works but the resulting image is darker than with regular path tracing. toggled with a button in UI.
(Describe what you did and, if there was a substantial amount of work involved, how you did it. If your extra features are interactive or can be toggled, describe how to use them.)

# Have you done extra credit work on previous rounds whose grading we have postponed to this round?

(Are all the features integrated into your Assn4 submission? If not, point out which one of your submissions we should use to grade those features.)

# Are there any known problems/bugs remaining in your code?

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
no
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course overall?
-I am still not sure how the probability distrubutions/importance samplign/MIS works. I didn't really manage to impelment anything and feel like i didn't understand this correctly at all. really hard to find info on the subjects, everything leads to Veach thesis and it is not on my level of understanding, and like a billion pages long.
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

