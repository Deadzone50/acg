#define _CRT_SECURE_NO_WARNINGS

#include "base/Defs.hpp"
#include "base/Math.hpp"
#include "RayTracer.hpp"
#include <stdio.h>
#include "rtIntersect.inl"
#include <fstream>

#include "rtlib.hpp"
#include <memory>
#include <algorithm>    // std::sort
#include <vector>
#include <stack>
#include <math.h>
#include "base/Random.hpp"

// Helper function for hashing scene data for caching BVHs
extern "C" void MD5Buffer( void* buffer, size_t bufLen, unsigned int* pDigest );


namespace FW
{


Vec2f getTexelCoords(Vec2f uv, const Vec2i size)
{

	Vec2f UVi = (Vec2i)uv;
	Vec2f Wrap = uv-(Vec2f)UVi;

	Wrap = Wrap + 1;
	Vec2f Wrapi = (Vec2i)Wrap;
	Vec2f Wrap2 = Wrap-(Vec2f)Wrapi;

	Vec2f Map = Wrap2*(Vec2f)size;

	return Map;

	//return Vec2f();
}

Mat3f formBasis(const Vec3f& n) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	Vec3f Q = n;
	if(abs(Q.x) <= abs(Q.y) && abs(Q.x) <= abs(Q.z))	//find smallest, replace with 1
		Q.x = 1;
	else if(abs(Q.y) <= abs(Q.x) && abs(Q.y) <= abs(Q.z))
		Q.y = 1;
	else
		Q.z = 1;

	Vec3f T = Q.cross(n).normalized();
	Vec3f B = n.cross(T);

	Mat3f R;

	R.setCol(0, T);
	R.setCol(1, B);
	R.setCol(2, n);
	return R;
    //return rtlib::formBasis(n);
}


String RayTracer::computeMD5( const std::vector<Vec3f>& vertices )
{
    unsigned char digest[16];
    MD5Buffer( (void*)&vertices[0], sizeof(Vec3f)*vertices.size(), (unsigned int*)digest );

    // turn into string
    char ad[33];
    for ( int i = 0; i < 16; ++i )
        ::sprintf( ad+i*2, "%02x", digest[i] );
    ad[32] = 0;

    return FW::String( ad );
}


// --------------------------------------------------------------------------


RayTracer::RayTracer()
{
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
    // After that this is not needed anymore.
    //m_rt.reset(new rtlib::RayTracer);
}

RayTracer::~RayTracer()
{
	for(auto it = BVH.begin(); it != BVH.end(); ++it)		//delete the reserved memory for the nodes
	{
		delete *it;
	}
	BVH.clear();
}

// Read a simple data type from a stream.
template<class T>
std::istream& read(std::istream& os, T& x)
{
	return os.read(reinterpret_cast<char*>(&x), sizeof(x));
}

void ReadNode(std::ifstream &file, Node* N)
{
	read(file, N->AABB[0].x);
	read(file, N->AABB[0].y);
	read(file, N->AABB[0].z);

	read(file, N->AABB[1].x);
	read(file, N->AABB[1].y);
	read(file, N->AABB[1].z);

	read(file, N->I1);
	read(file, N->I2);
}

void RayTracer::loadHierarchy(const char* filename, std::vector<RTTriangle>& triangles)
{
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	size_t TrigIsize, BVHsize;
	Node* N;

	std::ifstream file(filename, std::ios::binary);
	read(file, TrigIsize);
	TrigI.resize(TrigIsize);
	for(int i = 0; i < TrigIsize; ++i)
	{
		read(file, TrigI[i]);
	}
	read(file, BVHsize);

	for(int i = 0; i < BVHsize; ++i)
	{
		N = new Node;
		ReadNode(file, N);
		BVH.push_back(N);
	}
	int MaxDepth = (int)log2((FW::F32)BVHsize+1);
	int MSize = (int)TrigIsize;
	for(int i = 0; i < BVHsize; ++i)			//fix child pointers
	{
		int CSize = (int)(BVH[i]->I2 - BVH[i]->I1);		//current node size
		int Depth = (int)log2((FW::F32)MSize/CSize)+1;
		if(Depth < MaxDepth)
		{
			BVH[i]->LeftChild = BVH[i*2+1];
			BVH[i]->RightChild = BVH[i*2+2];
		}
		else
		{
			break;		//reached bottom row, no more child nodes
		}
	}
	file.close();
	m_triangles = &triangles;

	//m_rt->loadHierarchy(filename, triangles);
	//m_triangles = &triangles;
}
// Write a simple data type to a stream.
template<class T>
std::ostream& write(std::ostream& stream, const T& x)
{
	return stream.write(reinterpret_cast<const char*>(&x), sizeof(x));
}

void WriteNode(std::ofstream &file, Node* N)
{
	write(file, N->AABB[0].x);
	write(file, N->AABB[0].y);
	write(file, N->AABB[0].z);

	write(file, N->AABB[1].x);
	write(file, N->AABB[1].y);
	write(file, N->AABB[1].z);

	write(file, N->I1);
	write(file, N->I2);
}
void RayTracer::saveHierarchy(const char* filename, const std::vector<RTTriangle>& triangles) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	std::ofstream file(filename, std::ios::binary);
	write(file, TrigI.size());
	for(int i = 0; i < TrigI.size(); ++i)
	{
		write(file, TrigI[i]);
	}
	write(file, BVH.size());

	Node* N = BVH[0];
	WriteNode(file, N);
	std::deque<Node*> ToWrite;
	ToWrite.push_back(N);

	while(ToWrite.size() != 0)			//write one level at a time left to right
	{
		N = ToWrite.front();
		ToWrite.pop_front();
		if(N->LeftChild != NULL)
		{
			WriteNode(file, N->LeftChild);
			WriteNode(file, N->RightChild);

			ToWrite.push_back(N->LeftChild);
			ToWrite.push_back(N->RightChild);
		}
		else
		{				//reached bottom row
			break;
		}
	}
	file.close();

    //m_rt->saveHierarchy(filename, triangles);
}

static int Dim = 0;					//used for round robin selection
void RayTracer::constructHierarchy(std::vector<RTTriangle>& triangles, SplitMode splitMode) {
    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	Vec3f Min = FLT_MAX;			//object median
	Vec3f Max = -FLT_MAX;
	Node *N;
	if(TrigI.size() == 0)			//create indexlist if it does not exsist
	{
		for(int i = 0; i< triangles.size(); ++i)
			TrigI.push_back(i);
	}
	if(BVH.size() == 0)				//root node
	{
		N = new Node;
		N->I1 = 0;
		N->I2 = triangles.size();
		BVH.push_back(N);
	}
	else
	{
		N = BVH.back();
	}
	size_t i = N->I1;
	while(i != N->I2)				//check for longest dimensions
	{
		auto &T = triangles[TrigI[i]];
		Vec3f Val = T.min();
		if(Val.x < Min.x)
			Min.x = Val.x;
		if(Val.y < Min.y)
			Min.y = Val.y;
		if(Val.z < Min.z)
			Min.z = Val.z;

		Val = T.max();
		if(Val.x > Max.x)
			Max.x = Val.x;
		if(Val.y > Max.y)
			Max.y = Val.y;
		if(Val.z > Max.z)
			Max.z = Val.z;
		++i;
	}
	N->AABB[0] = Min;					//AABB
	N->AABB[1] = Max;
	//DrawAABB(*N);
	if(N->I2 - N->I1 < 5)				//stop if there are few triangles left
	{
		N->LeftChild = NULL;
		N->RightChild = NULL;
		//std::cout << "leaf\n";
		return;
	}
	float X = 0;
	float Y = 0;
	float Z = 0;
	//X = Max.x - Min.x;			//lenght of dimensions
	//Y = Max.y - Min.y;
	//Z = Max.z - Min.z;
	//Random R;					//use random dimension
	//X = R.getF32();
	//Y = R.getF32();
	//Z = R.getF32();
	if(Dim%3 == 0)			//round robin
		X = 1;
	else if(Dim%3 == 1)
		Y = 1;
	else //if(Dim%3 == 2)
		Z = 1;
	++Dim;

	if(X >= Y && X >= Z)				//sort objects based on their centerpoint along the longest axis
		std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().x < triangles[i2].centroid().x; });	//up to not including I2
	else if(Y >= X && Y >= Z)
		std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().y < triangles[i2].centroid().y; });
	else //if(Z >= Y && Z >= X)
		std::sort(TrigI.begin() + N->I1, TrigI.begin()+ N->I2, [&triangles](int i1, int i2) {return triangles[i1].centroid().z < triangles[i2].centroid().z; });

	N->LeftChild = new Node;
	N->LeftChild->I1 = N->I1;
	N->LeftChild->I2 = (N->I2 + N->I1)/2;
	BVH.push_back(N->LeftChild);
	constructHierarchy(triangles, splitMode);	//recursion

	N->RightChild = new Node;
	N->RightChild->I1 = (N->I2 + N->I1)/2;
	N->RightChild->I2 = N->I2;
	BVH.push_back(N->RightChild);
	constructHierarchy(triangles, splitMode);	//recursion

	m_triangles = &triangles;
	//m_rt->constructHierarchy(triangles, splitMode);
	//m_triangles = &triangles;
}

struct Hit
{
	bool H;
	float T1;
	float T2;
};

Hit
IntersectsNode(const Node& N, const Vec3f& orig, const Vec3f& InvDir, Vec3i& Swap, float TMax)		//test if ray hits a node
{
	float T1X = (N.AABB[Swap[0]].x - orig.x)*InvDir.x;		//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T2X = (N.AABB[1-Swap[0]].x - orig.x)*InvDir.x;

	float T1Y = (N.AABB[Swap[1]].y - orig.y)*InvDir.y;		//intersection y
	float T2Y = (N.AABB[1-Swap[1]].y - orig.y)*InvDir.y;
	if(T1X < T1Y)				//max of mins
		T1X = T1Y;
	if(T2X > T2Y)				//min of maxs
		T2X = T2Y;

	float T1Z = (N.AABB[Swap[2]].z - orig.z)*InvDir.z;		//intersection z
	float T2Z = (N.AABB[1-Swap[2]].z - orig.z)*InvDir.z;

	if(T1X < T1Z)				//max of mins
		T1X = T1Z;
	if(T2X > T2Z)				//min of maxs
		T2X = T2Z;

	if(T1X > TMax)				//check that it isn't too far
		return Hit{false};
	if(T1X > T2X)				//miss
	{
		return Hit{false};
	}
	else if(T2X < 0)			//behind, miss
	{
		return Hit{false};
	}
	else if(T1X > 0)			//hit
	{
		return Hit{true, T1X, T2X};
	}
	else
	{							//origin inside box
		return Hit{true, 0, T2X};
	}
}

RaycastResult RayTracer::raycast(const Vec3f& orig, const Vec3f& dir, std::vector<Node*> &ToCheck) const
{
	++m_rayCount;

    // YOUR CODE HERE (R1):
    // Integrate your implementation here.
	//int Trig = 0;				//used for checking efficency of BVH
	//int Thit = 0;
	//int Lhit = 0;
	//int Rhit = 0;
	//int Bhit = 0;

	Hit H, HL, HR;
	int imin = -1;
	float tmin = 1.0f, umin = 0.0f, vmin = 0.0f;

	Vec3f InvDir;					//precompute inverse of direction
	InvDir[0] = 1.0f/dir[0];		//floating point division by zero defined
	InvDir[1] = 1.0f/dir[1];
	InvDir[2] = 1.0f/dir[2];

	Vec3i Swap;						//precalculate if the bounds have to be swapped
	Swap.x = (dir.x < 0);
	Swap.y = (dir.y < 0);
	Swap.z = (dir.z < 0);

	H = IntersectsNode(*BVH[0], orig, InvDir, Swap, tmin);
	if(H.H)							//check if hit root node
	{
		ToCheck.push_back(BVH[0]);
		while(ToCheck.size() != 0)
		{
			Node* N = ToCheck.back();
			ToCheck.pop_back();
			if(N->LeftChild != NULL)	//check if leafnode
			{
				HL = IntersectsNode(*N->LeftChild, orig, InvDir, Swap, tmin);
				HR = IntersectsNode(*N->RightChild, orig, InvDir, Swap, tmin);
				//DrawAABB(*N->LeftChild);
				//DrawAABB(*N->RightChild);

				if(HL.H && HR.H)			//if both hit, check closest first
				{
					//Bhit++;
					if(HL.T1 < HR.T1)
					{
						ToCheck.push_back(N->RightChild);
						ToCheck.push_back(N->LeftChild);
					}
					else
					{
						ToCheck.push_back(N->LeftChild);
						ToCheck.push_back(N->RightChild);
					}
				}
				else if(HL.H)
				{
					//Lhit++;
					ToCheck.push_back(N->LeftChild);
				}
				else if(HR.H)
				{
					//Rhit++;
					ToCheck.push_back(N->RightChild);
				}
				else
				{
					//miss
				}
			}
			else
			{		//leafnode, check triangles
				for(size_t i = N->I1; i < N->I2; ++i)
				{
					//++Trig;		//count number of triangle tested per ray
					float t, u, v;
					if((*m_triangles)[TrigI[i]].intersect_woop(orig, dir, t, u, v))
					{
						if(t > 0.0f && t < tmin)
						{
							imin = (int)i;
							tmin = t;
							umin = u;
							vmin = v;
						}
					}
				}
			}
		}
	}
	//std::cout << "Triangles read:" << Trig << std::endl;
	//Thit = Bhit+Lhit+Rhit;
	//int tot = BVH.size();
	//std::cout << "Hits B, L, R, T:" << Bhit << ", " << Lhit << ", " << Rhit << ", " << Thit << ", " << tot << std::endl;
	RaycastResult castresult;
	if(imin != -1)
	{
		castresult = RaycastResult(&(*m_triangles)[TrigI[imin]], tmin, umin, vmin, orig + tmin*dir, orig, dir);
	}
	return castresult;

    //return m_rt->raycast(orig, dir);
}


} // namespace FW