# ME-E4100 Advanced Computer Graphics, Spring 2016
# Lehtinen / Kemppinen, Ollikainen
#
# Assignment 1: Accelerated Ray Tracing

Student name:Viktor M�sala
Student number:84732N

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1 BVH construction and traversal (5p): done		12h
        R2 BVH saving and loading (1p): done		5h
              R3 Simple texturing (1p): done		2h
             R4 Ambient occlusion (2p): done		3h
         R5 Simple multithreading (1p): done		0h

# Did you do any extra credit work?
-Tried to optimize my tracer... still super slow

(Describe what you did and, if there was a substantial amount of work involved, how you did it. If your extra features are interactive or can be toggled, describe how to use them.)

# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
no

# Any other comments you'd like to share about the assignment or the course so far?


